module.exports = function (io, loc) {
  //ネームスペースの宣言
  let chat = io.of('/chat');

  // チャット機能
  // connectionイベントを受信する
  chat.on('connection', function (socket) {

    // roomへの入室は、「socket.join(room名)」
    socket.on('client_to_server_join', function (data) {
      socket.join(data.room);

      chat.to(socket.id).emit('server_to_client', {
        value: data.room + 'に入室しました。'
      });
    });


    // client_to_serverイベント・データを受信する
    socket.on('client_to_server', function (data) {
      // S06. server_to_clientイベント・データを送信する
      chat.to(data.room).emit('server_to_client', {
        value: data.value
      });
    });


    // client_to_server_broadcastイベント・データを受信し、送信元以外に送信する
    socket.on('client_to_server_broadcast', function (data) {
      socket.broadcast.to(data.room).emit('server_to_client', {
        value: data.value
      });
    });


    // client_to_server_personalイベント・データを受信し、送信元のみに送信する
    socket.on('client_to_server_personal', function (data) {
      let personalMessage = "あなたは、" + data.user + "さんとして入室しました。"
      chat.to(socket.id).emit('server_to_client', {
        value: personalMessage
      });
    });


    // dicconnectイベントを受信し、退出メッセージを送信する
    socket.on('disconnect', function () {
      var user = loc.GetUserProperties(socket.id);

      if (user === undefined) {
        console.log("未入室のまま、どこかへ去っていきました。");
      } else {
        let endMessage = user + "さんが退出しました。"
        chat.to(data.room).emit('server_to_client', {
          value: endMessage
        });
      }
    });


  });
}