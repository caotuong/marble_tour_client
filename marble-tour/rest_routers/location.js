module.exports = function (io, loc) {
  let location = io.of('/location');

  location.on('connection', function (socket) {

    // roomへの入室は、「socket.join(room名)」
    socket.on('client_to_server_join', function (data) {
      socket.join(data.room);

      location.to(socket.id).emit('server_to_client', {
        value: "connected location socket!"
      });

      socket.to(socket.id).emit('server_to_client', {
        value: loc.Registration(data.room, data.user, data.group, data.latlng, data.targetId, data.targetLatLng, socket.id)
      });

      socket.to(socket.id).emit('server_to_client', {
        value: "connect location"
      });
    });
    
    // roomへの入室は、「socket.join(room名)」
    socket.on('client_to_server_location', function (data) {

      //      console.log(data);
      location.to(socket.id).emit('server_to_client', {
        value: loc.Registration(data.room, data.user, data.group, data.latlng, data.targetId, data.targetLatLng, socket.id)
      });

      {
        var managerId = loc.GetAdminSocketId(data.room, data.group);
        if (managerId !== undefined) {
          location.to(managerId).emit('server_to_client', {
            value: loc.Registration(data.room, data.user, data.group, data.latlng, data.targetId, data.targetLatLng, socket.id)
          });
        }
      }
      //      var admin = loc.
      //     location.to()
    });

    // dicconnectイベントを受信し、退出メッセージを送信する
    socket.on('disconnect', function () {
      var user = loc.GetUserProperties(socket.id);

      if (user !== undefined) {
        const name = Object.keys(user)[0];
        console.log("to offline :" + name);
        loc.ToOffline(socket.id);
      }
    });
  });
}
