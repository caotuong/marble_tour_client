module.exports = function (io, loc) {
  let fortune = io.of('/fortune');

  fortune.on('connection', function (socket) {
    let id = socket.id;
    // 運勢の配列からランダムで取得してアクセスしたクライアントに送信する
    let fortunes = ["大吉", "吉", "中吉", "小吉", "末吉", "凶", "大凶"];
    let selectedFortune = fortunes[Math.floor(Math.random() * fortunes.length)];
    let todaysFortune = "今日のあなたの運勢は… " + selectedFortune + " です。"
    fortune.to(id).emit('server_to_client', {
      value: todaysFortune
    })
  });
}