var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require("connect-flash");
var session = require("express-session");
var mongoose = require("mongoose");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var MasterUser = require("./models/user.js");
var route_config = require('./routes/config');
var tunnel = require('tunnel-ssh');

var http = require('http');
var socketio = require('socket.io');
var Locations = require('./locations')
var socket_router = require('./SOCKET');
var fs = require('fs');

//ソケットの宣言
function SOCKET() {
    let self = this;
    self.configSocketio();
}

//Socket.ioの設定
SOCKET.prototype.configSocketio = function () {
    let self = this;
    var loc = new Locations();

    self.startServer(loc);
}

//サーバーのスタート
SOCKET.prototype.startServer = function (loc) {

    let server = http.createServer(function (req, res) {
        var stream = fs.createReadStream('index.html');
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        stream.pipe(res);
    }).listen(3001, () => {
        console.log("Start Chat Service port:3001");
    }); // ポート競合の場合は値を変更

    //Socktのルーター
    let io = socketio.listen(server);
    var socket = new socket_router(io, loc);

}

//サーバーの停止
SOCKET.prototype.stop = function (err) {
    console.log(err);
    process.exit(1);
}

//ソケットの初期化
new SOCKET();


// MongoDB 接続先設定
//mongoose.connect("mongodb://localhost/marble-tour");
// mongoose.connect("mongodb://54.65.163.14/marble-tour");

//ローカル開発時
var config = {
   username: 'ec2-user',
   host: '54.65.163.14',
   //agent : process.env.SSH_AUTH_SOCK,
   //********プライベートキーのパスを入力
   privateKey: require('fs').readFileSync('./marble_Tour.pem'),
   　
   port: 22,
   //dstHost:'mongodb://localhost/marble-tour',
   dstPort: 27017,
   //localHost:'127.0.0.1',
   //password:'mypassword',
   localPort: 27017
};

var mgServer = tunnel(config, function (error, server) {
   if (error) {
       console.log("SSH 接続エラー: " + error);
   }
   //    mongoose.connect('mongodb://localhost:27017/mydbname');
   mongoose.connect("mongodb://localhost/marble-tour");

   var db = mongoose.connection;
   db.on('error', console.error.bind(console, 'DB 接続エラー:'));
   db.once('open', function () {
       // we're connected!
       console.log("DB 接続 成功");
   });
});

// passport が ユーザー情報をシリアライズすると呼び出されます
passport.serializeUser(function (id, done) {
    done(null, id);
});

// passport が ユーザー情報をデシリアライズすると呼び出されます
passport.deserializeUser(function (id, done) {
    MasterUser.findById(id, (error, user) => {
        if (error) {
            return done(error);
        }
        done(null, user);
    });
});

// passport における具体的な認証処理を設定します。
passport.use("local-login", new LocalStrategy({
        usernameField: "userId",
        passwordField: "password",
        passReqToCallback: true
    },
    function (req, username, password, done) {
        MasterUser.findOne({
            userId: username
        }, function (err, user) {
            if (err) {
                return done(err);
            }

            if (!user) {
                return done(null, false, {
                    message: 'ユーザー名かパスワードが無効です'
                });
            }
            user.validatePassword(password, function (err, isMatch) {
                if (err) {
                    return done(err);
                }
                if (!isMatch) {
                    return done(null, false, {
                        message: 'ユーザー名かパスワードが無効です'
                    });
                }
                if (user.role === 1 && user.loginStatus === 0) {
                    return done(null, false, {
                        message: 'このアカウントでログインする権限がありません。詳細については、管理者にお問い合わせください。'
                    });
                }
                if (user.role === 1 && Date.now() > user.activePeriod) {
                    return done(null, false, {
                        message: 'あなたの許可が失効しました。詳細については、管理者に連絡してください。'
                    });
                }
                return done(null, user);
            });
        });
    }
));


// 認可処理。指定されたロールを持っているかどうか判定します。
var authorize = function (role) {
    return function (request, response, next) {
        var requestedUrl = request.protocol + '://' + request.get('Host') + request.url;
        if (request.isAuthenticated() &&
            request.user.role === role) {
            return next();
        }
        response.redirect("/account/login");
    };
};

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


// ミドルウェアの設定
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));


//passoprt 設定
app.use(session({
    secret: "some salt",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

//for API
require('./routes/index')(app);

// ルーティング設定
app.use("/", (function () {
    var router = express.Router();

    router.get('/', function (request, response) {
        response.redirect('/home/index');
    });

    router.get("/home/index", function (request, response) {
        console.log("Test");
        response.render("./home/index");
    });
    router.get("/account/login", function (request, response) {
        if (request.isAuthenticated()) {
            response.redirect('/account/authorize');
        } else {
            response.render("./account/login", {
                message: request.flash('error')[0],
                isAuthenticated: request.isAuthenticated()
            });
        }
    });
    router.post("/account/login", passport.authenticate(
        "local-login", {
            successRedirect: "/account/authorize",
            failureRedirect: "/account/login",
            failureFlash: true
        }));
    router.post("/account/logout", function (request, response) {
        request.logout();
        response.redirect("/account/login");
    });
    router.get("/account/profile", authorize(0), function (request, response) {
        response.render("./account/profile", {
            user: request.user,
            isAuthenticated: request.isAuthenticated()
        });
    });

    // Authorization and redirect for each type of user
    router.get("/account/authorize", function (request, response) {
        if (request.isAuthenticated() && request.user) {
            console.log(request.user.role);
            switch (request.user.role) {
                case 0:
                    response.redirect('/admin' + route_config.admin_list_constractor);
                    break;
                case 1:
                    response.redirect('/manager/system');
                    break;
                default:
                    response.redirect('/manager/user_maps');
                    break;
            }
        }
    });

    return router;
})());

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;