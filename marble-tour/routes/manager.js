var User = require('../models/user.js');
var ManagerController = require('../controllers/managerController');

var route_config = require('../routes/config');

var authorize = function (role) {
    return function (request, response, next) {
        var requestedUrl = request.protocol + '://' + request.get('Host') + request.url;
        console.log(request.url);
        if (request.isAuthenticated() &&
            request.user.role === role) {
            return next();
        }
        response.redirect("/account/login?redirectUrl=" + request.url);
    };
};

module.exports = function (router) {
    //render System
    router.get(route_config.manager_system, authorize(1), ManagerController.renderManagerSystem);
    //get list group/users
    router.get(route_config.manager_list_group_user, authorize(1), ManagerController.listUserGroup);
    
    // Create user in group
    router.post(route_config.manager_create_user_group, ManagerController.createUserGroup);
    // Delete user in group
    router.get(route_config.manager_delete_user_group, authorize(1), ManagerController.deleteUser);
    // Update user in group
    router.post(route_config.manager_update_user_group, ManagerController.updateUserGroup);
    // Update group
    router.post(route_config.manager_update_group, ManagerController.updateGroup);
    // Delete user group
    router.get(route_config.manager_delete_group, ManagerController.deleteUserGroup);

    //======================
    //Manager Spots
    //======================
    // List spot group
    router.get(route_config.manager_list_spot_group, authorize(1), ManagerController.listSpotsGroup);
    // Create spot
    router.post(route_config.manager_create_spot_group, ManagerController.createSpot);
    // Delete spot
    router.get(route_config.manager_delete_spot_group, authorize(1), ManagerController.deleteSpot);
    // Update spot
    router.post(route_config.manager_update_spot_group, ManagerController.updateSpot);
    // Update group
    router.post(route_config.manager_update_spotgroup, ManagerController.updateSpotGroup);
    // Delete group
    router.get(route_config.manager_delete_spotgroup, ManagerController.deleteSpotGroup);
    //Get spotName
    router.get(route_config.get_spot_info, ManagerController.getSpotBySpotId);
    //Get group Id
    router.get(route_config.get_spotgroup_name_by_id, ManagerController.getSpotGroupNameById);
    //Get mark
    router.get(route_config.get_marker_by_spotgroup_name, ManagerController.getMarkerBySpotGroupName);

    //======================
    //Manager Users
    //======================
    //Get user Manager
//    router.get(route_config.manager_user_location, authorize(1), ManagerController.user_location);
//    router.get(route_config.manager_user_location, authorize(1), ManagerController.listUserGroup);
    
    router.get('/location', authorize(1), ManagerController.user_location);

    router.get('/user_maps', authorize(2), ManagerController.user_maps);

    router.get('/client_do_it', function (req, res) {
        res.render('manager/blank', {
            isAuthenticated: req.isAuthenticated(),
            isManager: req.user ? req.user.role == 1 : false
        });
    });

    return router;
}