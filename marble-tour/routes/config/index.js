'use strict';

var init = function () {

	return {
        ERROR:'error',
        SUCCESS: 'success',
        users: '/users',
        admin: '/admin',
        manager: '/manager',
        system_manager_render: './manager/system',
        manager_spot_group_render: './manager/spot_group',
        manger_user_render: './users/manage_user',
        manager_user_group_render:'./users/user_group',
        get_user_info: '/user/:id',
        get_group_name_by_id: '/group/:groupId',
        get_marker_by_group_name: '/groupname/:groupName',
        admin_list_constractor: '/contractors',
        admin_create_constractor: '/contractor/create',
        admin_delete_contractor: '/contractor/delete/:userId',
        admin_get_contractor: '/contractor/:id',
        admin_update_contractor: '/contractor/update/:id',
        admin_render_manager_user: './admin/manage_user',
        manager_list_group_user: '/user_group',
        manager_create_user_group: '/user/create',
        manager_system: '/system',
        manager_user_group: '/manager/user_group',
        manager_spot_group: '/manager/spot_group',
        manager_list_spot_group: '/spot_group',
        manager_delete_user_group: '/user/delete/:id',
        manager_create_spot_group: '/spot/create',
        manager_delete_spot_group: '/spot/delete/:id',
        manager_update_spot_group: '/spot/update/:id',
        manager_update_spotgroup: '/spotgroup/update/:groupId',
        manager_delete_spotgroup: '/spotgroup/delete/:groupId',
        get_spot_info: '/spot/:id',
        get_spotgroup_name_by_id: '/spot_group/:groupId',
        get_marker_by_spotgroup_name: '/spotgroupname/:groupName',
        manager_update_user_group: '/user/update/:id',
        manager_update_group: '/group/update/:groupId',
        manager_delete_group: '/group/delete/:groupId',
        manager_render_group:'./manager/user_group',
        manager_user: '/manage_user',
        manager_user_location: '/manager/location',
        manager_render_user_location: 'manager/users_location',

        user_maps: '/user_maps',
        user_render_maps: 'user/maps',
        
    }
}

module.exports = init();
