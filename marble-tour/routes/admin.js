var AdminController = require('../controllers/adminController');
var route_config = require('../routes/config');

var authorize = function (role) {
  return function (request, response, next) {
      if (request.isAuthenticated() &&
          request.user.role === role) {
          return next();
      }
      response.redirect("/account/login");
  };
};

module.exports = function(router){
 
  router.get(route_config.admin_list_constractor, authorize(0), AdminController.listContractors);

  router.post(route_config.admin_create_constractor, AdminController.createContractor);

  router.get(route_config.admin_delete_contractor, authorize(0), AdminController.deleteContractor);

  router.get(route_config.admin_get_contractor, AdminController.getContractor);

  router.post(route_config.admin_update_contractor, AdminController.updateContractor);

  return router;
}