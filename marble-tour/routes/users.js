var User = require('../models/user.js');
var UserController = require('../controllers/userController');
var AdminController = require('../controllers/adminController');

var route_config = require('../routes/config');

var authorize = function (role) {
  return function (request, response, next) {
      if (request.isAuthenticated() &&
          request.user.role === role) {
          return next();
      }
      response.redirect("/account/login");
  };
};

module.exports = function(router){
 //====================
  //Insert Admin account only
  //====================
  router.post('/insert',function(req,res){
	UserController.insert(req,res);
  });

  //====================
  //END Insert Admin account
  //====================

  router.get(route_config.get_user_info, UserController.getUserByUserId);

  router.get(route_config.get_group_name_by_id, UserController.getGroupNameById);

  router.get(route_config.get_marker_by_group_name, UserController.getMarkerByGroupName);

  router.get(route_config.manager_user, authorize(1), UserController.manageUser);

  


  return router;
}