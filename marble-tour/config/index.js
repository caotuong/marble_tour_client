'use strict';

var init = function () {

	return {
        KEY: 'marble-tour',
        marks: [
            {
                mark: 'Mark 1',
                path: '/images/mark1.png'
            },
            {
                mark: 'Mark 2',
                path: '/images/mark2.png'
            },
            {
                mark: 'Accountancy',
                path: '/images/accountancy.png'
            },
            {
                mark: 'arts-crafts',
                path: '/images/arts-crafts.png'
            },
            {
                mark: 'astrology',
                path: '/images/astrology.png'
            },
            {
                mark: 'automotive',
                path: '/images/automotive.png'
            },
            {
                mark: 'bars',
                path: '/images/bars.png'
            },
            {
                mark: 'birds',
                path: '/images/birds.png'
            }
            
        ]
        
    }
}

module.exports = init();
