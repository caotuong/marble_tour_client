var init = function () {
    
        return {
            //COMMON
            // error_create: 'Error when create ',
            error_create: '作成に失敗しました ',
            // error_delete: 'Error when delete ',
            error_delete: '削除に失敗しました ',
            // error_update: 'Error when update ',
            error_update: '更新に失敗しました ',
            // success_delete: 'Deleted successfully',
            success_delete: '正常に削除されました',
            // success_update: 'Updated successfully',
            success_update: '正常に更新されました',
            // success_create: 'Created successfully',
            success_create: '正常に作成されました',
            

            //USER
            // user: 'User ',
            user: 'ユーザー ',
            // invalid_username_password: 'Invalid username or password ',
            invalid_username_password: 'ユーザー名かパスワードが無効です ',
            // has_been_created:'has been created',
            has_been_created:'作成されました',
            // error_get_user:'Error when get user, please try again.',
            error_get_user:'ユーザーの取得に失敗しました。もう一度お試しください',
            // error_duplicate_userid:'Duplicate user Id, please choose an another user Id',
            error_duplicate_userid:'ユーザーIDが重複しています。別のユーザーIDを選択してください',
            // error_missing_userid: 'Error: Missing user Id.',
            error_missing_userid: 'エラー：ユーザーIDがありません',
            // error_already_exist_user: 'User already exist ',
            error_already_exist_user: 'ユーザーは既に存在します ',
            // error_cannotfound_user: 'Can not found user.',
            error_cannotfound_user: 'ユーザーが見つかりません',

            //GROUP
            // group: 'Group ',
            group: 'グループ ',
            // error_groupname_empty:'Error: GroupName empty',
            error_groupname_empty:'エラー：グループ名が空です',
            // invalid_groupname: 'Invalid GroupName',
            invalid_groupname: 'グループ名が無効です',
            // error_find_groupname:'Error when find userGroupName',
            error_find_groupname:'ユーザーグループ名が見つかりませんでした',
            // deleted: 'deleted',
            deleted: '削除されました',
            // error_get_groupname:'Error when get group name by group Id',
            error_get_groupname:'グループIDからのグループ名取得に失敗しました',
            // error_get_mark:'Error when get marker by group name.',
            error_get_mark:'グループのマーカー取得に失敗しました',

            //CONTRACTOR
            // contractor: 'Contractor ',
            contractor: '契約者 ',
            // error_create_contractor: 'Error when create contractor, please try again.',
            error_create_contractor: '契約者の作成に失敗しました。もう一度お試しください',
            // error_delete_contractor: 'Error when create contractor, please try again.',
            error_delete_contractor: '契約者の削除に失敗しました。もう一度お試しください',
            // error_get_contractor: 'Error when get contractor, please try again.',
            error_get_contractor: '契約者の取得に失敗しました。もう一度お試しください',
            // error_update_contractor: 'Error when update contractor, please try again.',
            error_update_contractor: '契約者の更新に失敗しました。もう一度お試しください',

            //MANAGER SYSTEM
            system_manager_group_users : "参加者、グループ管理",
            system_manager_group_spots : "観光地管理",
            system_manager_users : "ユーザー管理",

            //SPOTS
            // error_already_exist_spot:'Spot already exist. ',
            error_already_exist_spot:'スポットは既に存在します',
            // error_missing_spotname: 'Error: Missing spotName.',
            error_missing_spotname: 'エラー：スポット名が存在しません',
            // error_cannot_found_spotname: 'Can not found spotName.',
            error_cannot_found_spotname: 'スポット名が見つかりません',
            
        }
    }
    
    module.exports = init();