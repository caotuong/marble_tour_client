var validator = require('validator');
var route_config = require('../routes/config');

exports.validateContractor = function(contractor, req, res) {

    var success = true;

    if (validator.isEmpty(contractor.userId)) {
        req.flash(route_config.ERROR, 'ユーザーIDが入力されていません');
        success = false;
    } else {
        if (!validator.isHalfWidth(contractor.userId)) {
            req.flash(route_config.ERROR, 'ユーザーIDには、全角文字は利用できません');
            success = false;
        }
        if (contractor.userId.length > 128) {
            req.flash(route_config.ERROR, 'ユーザーIDは128文字以下で入力してください');
            success = false;
        }

        if (hasWhiteSpace(contractor.userId)) {
            req.flash(route_config.ERROR, 'ユーザーIDにはスペースを使用できません');
            success = false;
        }
        
    }

    if (validator.isEmpty(contractor.password)) {
        req.flash(route_config.ERROR, 'パスワードが入力されていません');
        success = false;
    }

    if (validator.isEmpty(contractor.email)) {
        req.flash(route_config.ERROR, 'メールアドレスが入力されていません');
        success = false;
    } else if (!validator.isEmail(contractor.email)) {
        req.flash(route_config.ERROR, '正しいメールアドレスを入力してください');
        success = false;
    }


    if (validator.isEmpty(contractor.phone)) {
        req.flash(route_config.ERROR, '電話番号が入力されていません');
        success = false;
    } else {
        if (!isPhoneNumber(contractor.phone)) {
            req.flash(route_config.ERROR, '電話番号：ハイフン無しの形式で、正しい電話番号を入力してください');
            success = false; 
        }
    }
    
    if (contractor.activePeriod === null) {
        req.flash(route_config.ERROR, '有効期限が入力されていません');
        success = false;
    }

    

    return success;
};

function hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
}

function isPhoneNumber(phone) {
    var valid = true;
    if(phone && phone.length > 0) {
        for (var i=0; i<phone.length; i++) {
            if (!(phone[i] == '-' || validator.isNumeric(phone[i]))) {
                valid = false;
            }
        }
    } else {
        valid = false;
    }

    return valid;
}

exports.validateUserGroup = function(userGroup, req, res) {
    var valid = true;
    
    if (validator.isEmpty(userGroup.userId)) {
        req.flash(route_config.ERROR, 'ユーザーIDが入力されていません');
        valid = false;
    }
    if (!validator.isHalfWidth(userGroup.userId)) {
        req.flash(route_config.ERROR, 'ユーザーIDには、全角文字は利用できません');
        valid = false;
    }

    if (hasWhiteSpace(userGroup.userId)) {
        req.flash(route_config.ERROR, 'ユーザーIDにはスペースを使用できません');
        valid = false;
    }


    if (validator.isEmpty(userGroup.userGroupName)) {
        req.flash(route_config.ERROR, "ユーザーのグループ名が入力されていません");
        valid = false;
    }

    if (validator.isEmpty(userGroup.password)) {
        req.flash(route_config.ERROR, 'パスワードが入力されていません');
        valid = false;
    }
    
    if (validator.isEmpty(userGroup.phone)) {
        req.flash(route_config.ERROR, '電話番号が入力されていません');
        valid = false;
    } else {
        if (!isPhoneNumber(userGroup.phone)) {
            req.flash(route_config.ERROR, '電話番号：ハイフン無しの形式で、正しい電話番号を入力してください');
            valid = false;
        }
    }

    return valid;
}

exports.validateUpdateUserGroup = function(userGroup, req, res, next) {
    var valid = true;
    if (!validator.isHalfWidth(userGroup.userId)) {
        req.flash(route_config.ERROR, 'ユーザーIDには、全角文字は利用できません');
        valid = false;
    }
    if (validator.isEmpty(userGroup.userId)) {
        req.flash(route_config.ERROR, 'ユーザーIDが入力されていません');
        valid = false;
    }
    if (validator.isEmpty(userGroup.userGroupName)) {
        req.flash(route_config.ERROR, 'ユーザーのグループ名が入力されていません');
        valid = false;
    }

    if (validator.isEmpty(userGroup.password)) {
        req.flash(route_config.ERROR, 'パスワードが入力されていません');
        valid = false;
    }

    if (validator.isEmpty(userGroup.phone)) {
        req.flash(route_config.ERROR, '電話番号が入力されていません');
        valid = false;
    } else {
        if (!isPhoneNumber(userGroup.phone)) {
            req.flash(route_config.ERROR, '電話番号：ハイフン無しの形式で、正しい電話番号を入力してください');
            valid = false;
        }
    }

    return valid;
};

exports.validateUpdateGroup = function(group, req, res) {
    var valid = true;
    if (validator.isEmpty(group.userGroupName)) {
        req.flash(route_config.ERROR, 'ユーザーのグループ名が入力されていません');
        valid = false;
    }

    if (validator.isEmpty(group.mark)) {
        req.flash(route_config.ERROR, 'グループマーカーが指定されていません');
        valid = false;
    }

    return valid;
};

exports.validateSpotGroup = function(spotGroup, req, res) {
        var valid = true;
    
        if (validator.isEmpty(spotGroup.spotGroupName)) {
            req.flash(route_config.ERROR, 'グループマーカーが指定されていません');
            valid = false;
        } 
    
        if (validator.isEmpty(spotGroup.spotName)) {
            req.flash(route_config.ERROR, 'フィールドスポット名が入力されていません');
            valid = false;
        }
    
        if (validator.isEmpty(spotGroup.mark)) {
            req.flash(route_config.ERROR, 'フィールドマーカーが指定されていません');
            valid = false;
        } 
    
       
        return valid;
};
exports.validateUpdateSpotInGroup = function(spotGroup, req, res, next) {
    var valid = true;
    if (validator.isEmpty(spotGroup.spotName)) {
        req.flash(route_config.ERROR, 'スポット名が入力されていません');
        valid = false;
    }
    if (validator.isEmpty(spotGroup.spotGroupName)) {
        req.flash(route_config.ERROR, 'スポットグループ名が入力されていません');
        valid = false;
    }

    return valid;
};
exports.validateUpdateSpotGroup = function(group, req, res) {
    var valid = true;
    if (validator.isEmpty(group.spotGroupName)) {
        req.flash(route_config.ERROR, 'スポットグループ名が入力されていません');
        valid = false;
    }

    if (validator.isEmpty(group.mark)) {
        req.flash(route_config.ERROR, 'グループマーカーが指定されていません');
        valid = false;
    }

    return valid;
};
    

