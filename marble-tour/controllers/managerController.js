var User = require('../models/user');
var Spot = require('../models/spot');
var route_config = require('../routes/config');
var validator = require('../utils/validator');
var CryptoJS = require("crypto-js");
var config = require('../config');
var uuid = require('uuid/v1');
var Language = require('../config/language');
var ObjectId = require('mongoose').Types.ObjectId;
//================================
// START Manger System
//================================
exports.renderManagerSystem = function (req, res, next) {
    var data = [{
            link: route_config.manager_user_group,
            title: Language.system_manager_group_users
        },
        {
            link: route_config.manager_spot_group,
            title: Language.system_manager_group_spots
        },
        {
            //link: 'client_do_it', title: Language.system_manager_users
            link: route_config.manager_user_location,
            title: Language.system_manager_users
        }
    ];
    res.render(route_config.system_manager_render, {
        links: data,
        isAuthenticated: req.isAuthenticated(),
        isManager: req.user.role == 1
    });
};
//================================
// END Manger System
//================================


//================================
// START Manger groups/users
//================================
// List group users
exports.listUserGroup = function (req, res, next) {
    User.find({
        'role': 2,
        'managerId': req.user.userId,
        'adminId': req.user.adminId
    }, function (err, users) {
        var groupArr = [];
        if (users && users.length > 0) {
            users.sort(function (a, b) {
                return a.userGroupName > b.userGroupName;
            });
            var tempGroupName = users[0].userGroupName;
            var tempGroupId = users[0].userGroupId;
            var tempMark = users[0].mark;
            var userArray = [];
            var count = 0;
            users.forEach(function (user) {
                count++;
                if (user.userGroupName !== tempGroupName) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        users: userArray
                    });
                    userArray = [];
                    tempGroupName = user.userGroupName;
                    tempGroupId = user.userGroupId;
                    tempMark = user.mark;
                }
                var bytes = CryptoJS.AES.decrypt(user.password, config.KEY);
                var dePass = bytes.toString(CryptoJS.enc.Utf8);

                userArray.push({
                    id: user._id,
                    userId: user.userId,
                    password: dePass,
                    phone: user.phone
                });
                if (count === users.length) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        users: userArray
                    });
                }
            });
        }

        res.render(route_config.manager_render_group, {
            groupUsers: groupArr,
            errors: req.flash(route_config.ERROR),
            success: req.flash(route_config.SUCCESS),
            isAuthenticated: req.isAuthenticated(),
            isManager: req.user.role == 1,
            marks: config.marks,
            managerId: req.user.managerId,
            adminId: req.user.adminId,
            userSession: req.flash('userSession')
        });
    });
}

// Create user group
exports.createUserGroup = function (req, res, next) {
    var credentials = {
        userId: req.body.userId,
        password: req.body.password,
        phone: req.body.phone,
        userGroupName: req.body.userGroupName,
        mark: req.body.mark,
        role: 2,
        userGroupId: uuid(),
        managerId: req.body.managerId,
        adminId: req.body.adminId
    };


    if (!validator.validateUserGroup(credentials, req, res)) {
        req.flash('userSession', {
            userId: credentials.userId,
            userGroupName: credentials.userGroupName,
            password: credentials.password,
            phone: credentials.phone,
            mark: credentials.mark
        });
        console.log('Not validate');
        res.redirect(route_config.manager + route_config.manager_list_group_user);
    } else {
        console.log('========== validate');
        User.findOne({
            'userId': credentials.userId
        }, function (err, user) {
            if (err) {
                req.flash(route_config.ERROR, Language.error_create);
                res.redirect(route_config.manager + route_config.manager_list_group_user);
            }
            if (user) {
                req.flash(route_config.ERROR, Language.error_already_exist_user + credentials.userId);
                res.redirect(route_config.manager + route_config.manager_list_group_user);
            } else {
                User.findOne({
                    userGroupName: req.body.userGroupName
                }, function (err, user) {
                    if (user && user.userGroupId) {
                        credentials.userGroupId = user.userGroupId;
                    }
                    User.create(credentials, function (err, newUser) {
                        if (err) {
                            req.flash(route_config.ERROR, Language.error_create);
                            res.redirect(route_config.manager + route_config.manager_list_group_user);
                        } else {
                            req.flash(route_config.SUCCESS, Language.success_create);
                            res.redirect(route_config.manager + route_config.manager_list_group_user);
                        }
                    });
                });
            }
        });
    }
}
// Delete user
exports.deleteUser = function (req, res, next) {

    User.findOne({
        '_id': new ObjectId(req.params.id)
    }, function (err, user) {
        if (err) {
            req.flash(route_config.ERROR, Language.error_delete);
            res.redirect(route_config.manager + route_config.manager_list_group_user);
        }
        if (user) {
            user.remove(function (err) {
                if (err) {
                    req.flash(route_config.ERROR, Language.error_delete);
                    res.redirect(route_config.manager + route_config.manager_list_group_user);
                } else {
                    req.flash(route_config.SUCCESS, Language.success_delete);
                    res.redirect(route_config.manager + route_config.manager_list_group_user);
                }
            });
        } else {
            req.flash(route_config.ERROR, Language.error_cannotfound_user);
            res.redirect(route_config.manager + route_config.manager_list_group_user);
        }

    });
};

exports.updateUserGroup = function (req, res, next) {
    var credentials = {
        'userId': req.body.userId,
        'password': req.body.password,
        'phone': req.body.phone,
        'userGroupName': req.body.userGroupName,
        'mark': req.body.mark
    };

    User.findOne({
        '_id': new ObjectId(req.params.id)
    }, function (err, user) {
        if (err) {
            req.flash(route_config.ERROR, Language.error_update);
            res.redirect(route_config.manager + route_config.manager_list_group_user);
        }
        if (user) {
            if (!validator.validateUpdateUserGroup(credentials, req, res)) {
                res.redirect(route_config.manager + route_config.manager_list_group_user);
            } else {
                user.password = credentials.password;
                user.userId = credentials.userId;
                user.phone = credentials.phone;
                user.mark = credentials.mark;

                if (user.userGroupName !== credentials.userGroupName) {
                    user.userGroupName = credentials.userGroupName;
                    user.userGroupId = uuid();
                }

                user.save(function (err) {
                    if (err) {
                        console.log(err);
                        req.flash(route_config.ERROR, Language.error_update);
                        res.redirect(route_config.manager + route_config.manager_list_group_user);
                    } else {
                        req.flash(route_config.SUCCESS, Language.success_update);
                        res.redirect(route_config.manager + route_config.manager_list_group_user);
                    }
                });
            }


        } else {
            req.flash(route_config.ERROR, Language.error_update);
            res.redirect(route_config.manager + route_config.manager_list_group_user);
        }
    });
};

exports.updateGroup = function (req, res, next) {
    var credentials = {
        'groupId': req.params.groupId,
        'mark': req.body.mark,
        'userGroupName': req.body.userGroupName,
    };

    if (!validator.validateUpdateGroup(credentials, req, res)) {
        res.redirect(route_config.manager + route_config.manager_list_group_user);
    } else {
        User.find({
            userGroupId: req.params.groupId
        }, function (err, users) {
            if (err) {
                req.flash(route_config.ERROR, Language.error_update);
                res.redirect(route_config.manager + route_config.manager_list_group_user);
            } else {
                if (users) {
                    users.forEach(function (user) {
                        user.mark = credentials.mark;
                        user.userGroupName = credentials.userGroupName;
                        user.save();
                    });
                }
                req.flash(route_config.SUCCESS, Language.success_update);
                res.redirect(route_config.manager + route_config.manager_list_group_user)
            }
        });
    }

}

exports.deleteUserGroup = function (req, res, next) {
    User.find({
        userGroupId: req.params.groupId
    }, function (err, users) {
        if (err) {
            req.flash(route_config.ERROR, Language.error_delete);
            res.redirect(route_config.manager + route_config.manager_list_group_user);
        } else {
            if (users) {
                console.log(users);
                users.forEach(function (user) {
                    user.remove();
                });
            }
            req.flash(route_config.SUCCESS, Language.success_delete);
            res.redirect(route_config.manager + route_config.manager_list_group_user);
        }
    });
}
//================================
// END Manger groups/users
//================================


//================================
// START Manger groups/spots
//================================
// List groups/spots
exports.listSpotsGroup = function (req, res, next) {
    Spot.find({
        managerId: req.user.userId
    }, function (err, spots) {
        var groupArr = [];
        if (spots && spots.length > 0) {
            spots.sort(function (a, b) {
                return a.spotGroupName > b.spotGroupName;
            });
            var tempGroupName = spots[0].spotGroupName;
            var tempGroupId = spots[0].spotGroupId;
            var tempMark = spots[0].mark;
            var spotArray = [];
            var count = 0;
            spots.forEach(function (spot) {
                count++;
                if (spot.spotGroupName !== tempGroupName) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        spots: spotArray
                    });
                    spotArray = [];
                    tempGroupName = spot.spotGroupName;
                    tempGroupId = spot.spotGroupId;
                    tempMark = spot.mark;
                }

                spotArray.push({
                    id: spot._id,
                    spotName: spot.spotName,
                    lat: spot.lat,
                    lon: spot.lon
                });
                if (count === spots.length) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        spots: spotArray
                    });
                }
            });
        }


        res.render(route_config.manager_spot_group_render, {
            groupSpots: groupArr,
            errors: req.flash(route_config.ERROR),
            success: req.flash(route_config.SUCCESS),
            isAuthenticated: req.isAuthenticated(),
            isManager: req.user.role == 1,
            marks: config.marks,
            managerId: req.user.managerId,
            adminId: req.user.adminId,
            spotSession: req.flash('spotSession')
        });
    });
}

// Create spot in group
exports.createSpot = function (req, res, next) {
    var credentials = {
        spotName: req.body.spotName,
        lat: req.body.lat,
        lon: req.body.lon,
        spotGroupName: req.body.spotGroupName,
        mark: req.body.mark,
        role: 2,
        spotGroupId: uuid(),
        managerId: req.body.managerId,
        adminId: req.body.adminId
    };


    if (!validator.validateSpotGroup(credentials, req, res)) {
        req.flash('spotSession', {
            spotGroupName: credentials.spotGroupName,
            mark: credentials.mark
        });
        res.redirect(route_config.manager + route_config.manager_list_spot_group);
    } else {
        Spot.findOne({
            'spotName': credentials.spotName,
            'managerId': req.user.userId
        }, function (err, spot) {
            if (err) {
                req.flash(route_config.ERROR, Language.error_create);
                res.redirect(route_config.manager + route_config.manager_list_spot_group);
            }
            if (spot) {
                req.flash(route_config.ERROR, Language.error_already_exist_spot + credentials.spotName);
                res.redirect(route_config.manager + route_config.manager_list_spot_group);
            } else {
                Spot.findOne({
                    spotGroupName: req.body.spotGroupName
                }, function (err, spot) {
                    if (spot && spot.spotGroupId) {
                        credentials.spotGroupId = spot.spotGroupId;
                    }
                    Spot.create(credentials, function (err, newSpot) {
                        if (err) {
                            req.flash(route_config.ERROR, Language.error_create);
                            res.redirect(route_config.manager + route_config.manager_list_spot_group);
                        } else {
                            req.flash(route_config.SUCCESS, Language.success_create);
                            res.redirect(route_config.manager + route_config.manager_list_spot_group);
                        }
                    });
                });
            }
        });
    }
}
// Delete spot in group
exports.deleteSpot = function (req, res, next) {
    Spot.findOne({
        '_id': new ObjectId(req.params.id)
    }, function (err, spot) {
        if (err) {
            req.flash(route_config.ERROR, Language.error_delete);
            res.redirect(route_config.manager + route_config.manager_list_spot_group);
        }
        if (spot) {
            spot.remove(function (err) {
                if (err) {
                    req.flash(route_config.ERROR, Language.error_delete);
                    res.redirect(route_config.manager + route_config.manager_list_spot_group);
                } else {
                    req.flash(route_config.SUCCESS, Language.success_delete);
                    res.redirect(route_config.manager + route_config.manager_list_spot_group);
                }
            });
        } else {
            req.flash(route_config.ERROR, Language.error_cannot_found_spotname);
            res.redirect(route_config.manager + route_config.manager_list_spot_group);
        }

    });
};

exports.updateSpot = function (req, res, next) {
    var credentials = {
        'spotName': req.body.spotName,
        'lat': req.body.lat,
        'lon': req.body.lon,
        'spotGroupName': req.body.spotGroupName,
        'mark': req.body.mark
    };

    Spot.findOne({
        '_id': new ObjectId(req.params.id),
        'managerId': req.user.userId
    }, function (err, spot) {
        if (err) {
            req.flash(route_config.ERROR, Language.error_update);
            res.redirect(route_config.manager + route_config.manager_list_spot_group);
        }
        if (spot) {
            if (!validator.validateUpdateSpotInGroup(credentials, req, res)) {
                res.redirect(route_config.manager + route_config.manager_list_spot_group);
            } else {
                spot.spotName = credentials.spotName;
                spot.lat = credentials.lat;
                spot.lon = credentials.lon;
                spot.mark = credentials.mark;

                if (spot.spotGroupName !== credentials.spotGroupName) {
                    spot.spotGroupName = credentials.spotGroupName;
                    spot.spotGroupId = uuid();
                }

                spot.save(function (err) {
                    if (err) {
                        req.flash(route_config.ERROR, Language.error_update);
                        res.redirect(route_config.manager + route_config.manager_list_spot_group);
                    } else {
                        req.flash(route_config.SUCCESS, Language.success_update);
                        res.redirect(route_config.manager + route_config.manager_list_spot_group);
                    }
                });
            }


        } else {
            req.flash(route_config.ERROR, Language.error_update);
            res.redirect(route_config.manager + route_config.manager_list_spot_group);
        }
    });
};

exports.updateSpotGroup = function (req, res, next) {
    var credentials = {
        'groupId': req.params.groupId,
        'mark': req.body.mark,
        'spotGroupName': req.body.spotGroupName,
    };

    if (!validator.validateUpdateSpotGroup(credentials, req, res)) {
        res.redirect(route_config.manager + route_config.manager_list_spot_group);
    } else {
        Spot.find({
            'spotGroupId': req.params.groupId
        }, function (err, spots) {
            if (err) {
                req.flash(route_config.ERROR, Language.error_update);
                res.redirect(route_config.manager + route_config.manager_list_spot_group);
            } else {
                console.log('==>' + spots);
                if (spots) {
                    spots.forEach(function (spot) {
                        spot.mark = credentials.mark;
                        spot.spotGroupName = credentials.spotGroupName;
                        spot.save();
                    });
                }
                req.flash(route_config.SUCCESS, Language.success_update);
                res.redirect(route_config.manager + route_config.manager_list_spot_group)
            }
        });
    }

}

exports.deleteSpotGroup = function (req, res, next) {
    Spot.find({
        'managerId': req.user.userId,
        'spotGroupId': req.params.groupId
    }, function (err, spots) {
        if (err) {
            req.flash(route_config.ERROR, Language.error_delete);
            res.redirect(route_config.manager + route_config.manager_list_spot_group);
        } else {
            if (spots) {
                console.log(spots);
                spots.forEach(function (spot) {
                    spot.remove();
                });
            }
            req.flash(route_config.SUCCESS, Language.success_delete);
            res.redirect(route_config.manager + route_config.manager_list_spot_group);
        }
    });
}

exports.getSpotBySpotId = function (req, res, next) {
    Spot.findOne({
        'managerId': req.user.userId,
        '_id': new ObjectId(req.params.id)
    }, function (err, spot) {
        if (err) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_cannot_found_spotname
            }));
        }
        if (spot) {
            res.end(JSON.stringify({
                success: true,
                spot: spot
            }));
        } else {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_cannot_found_spotname
            }));
        }

    });
}

exports.getSpotGroupNameById = function (req, res, next) {
    Spot.findOne({
        'spotGroupId': req.params.groupId
    }, function (err, spot) {
        if (err) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_groupname
            }));
        }
        if (spot) {
            res.end(JSON.stringify({
                success: true,
                spotGroupName: spot.spotGroupName,
                spotGroupId: spot.spotGroupId,
                mark: spot.mark,
                message: ''
            }));
        } else {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_groupname
            }));
        }
    });
};

exports.getMarkerBySpotGroupName = function (req, res, next) {
    Spot.findOne({
        'managerId': req.user.userId,
        'spotGroupName': req.params.groupName
    }, function (err, spot) {
        if (err || !spot) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_mark
            }));
        } else {
            res.end(JSON.stringify({
                success: true,
                mark: spot.mark,
                message: ''
            }));
        }
    });
};

//================================
// END Manger groups/spots
//================================


//================================
// START Manger groups/users
//================================
// List groups/users
exports.user_location = function (req, res, next) {

    User.find({
        'role': 2,
        'managerId': req.user.userId,
        'adminId': req.user.adminId
    }, function (err, users) {
        var groupArr = [];
        if (users && users.length > 0) {
            users.sort(function (a, b) {
                return a.userGroupName > b.userGroupName;
            });
            var tempGroupName = users[0].userGroupName;
            var tempGroupId = users[0].userGroupId;
            var tempMark = users[0].mark;
            var userArray = [];
            var count = 0;
            users.forEach(function (user) {
                count++;
                if (user.userGroupName !== tempGroupName) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        users: userArray
                    });
                    userArray = [];
                    tempGroupName = user.userGroupName;
                    tempGroupId = user.userGroupId;
                    tempMark = user.mark;
                }
                var bytes = CryptoJS.AES.decrypt(user.password, config.KEY);
                var dePass = bytes.toString(CryptoJS.enc.Utf8);

                userArray.push({
                    id: user._id,
                    userId: user.userId,
                    password: dePass,
                    phone: user.phone
                });
                if (count === users.length) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        users: userArray
                    });
                }
            });
        }

        Spot.find({
            managerId: req.user.userId
        }, function (err, spots) {
            var spotGroupArr = [];
            if (spots && spots.length > 0) {
                spots.sort(function (a, b) {
                    return a.spotGroupName > b.spotGroupName;
                });
                var tempGroupName = spots[0].spotGroupName;
                var tempGroupId = spots[0].spotGroupId;
                var tempMark = spots[0].mark;
                var spotArray = [];
                var count = 0;
                spots.forEach(function (spot) {
                    count++;
                    if (spot.spotGroupName !== tempGroupName) {
                        spotGroupArr.push({
                            groupName: tempGroupName,
                            groupId: tempGroupId,
                            mark: tempMark,
                            spots: spotArray
                        });
                        spotArray = [];
                        tempGroupName = spot.spotGroupName;
                        tempGroupId = spot.spotGroupId;
                        tempMark = spot.mark;
                    }

                    spotArray.push({
                        id: spot._id,
                        spotName: spot.spotName,
                        lat: spot.lat,
                        lon: spot.lon
                    });
                    if (count === spots.length) {
                        spotGroupArr.push({
                            groupName: tempGroupName,
                            groupId: tempGroupId,
                            mark: tempMark,
                            spots: spotArray
                        });
                    }
                });
            }

            res.render(route_config.manager_render_user_location, {
                userName : req.user.userId,
                roomName: req.user.userId,
                groupUsers: groupArr,
                groupSpots: spotGroupArr,
                errors: req.flash(route_config.ERROR),
                success: req.flash(route_config.SUCCESS),
                isAuthenticated: req.isAuthenticated(),
                isManager: req.user.role == 1,
                marks: config.marks,
                managerId: req.user.managerId,
                adminId: req.user.adminId,
                userSession: req.flash('userSession')
            });
        });
        // Spot.findOne({'managerId': req.user.userId}, function(err, spot){
        //     if (err || !spot) {
        //         res.end(JSON.stringify({
        //             success: false,
        //             message: Language.error_get_mark
        //         }));
        //     } else{ 
        //         res.end(JSON.stringify({
        //             success: true,
        //             mark: spot.mark,
        //             message: ''
        //         }));
        //     }
        // });
    })
}

//================================
// END Manger groups/users
//================================

exports.user_maps = function (req, res, next) {

    User.find({
        'role': 2,
        'managerId': req.user.managerId,
        //'userId': req.user.Id,
        'adminId': req.user.adminId
    }, function (err, users) {
        var groupArr = [];
        if (users && users.length > 0) {
            users.sort(function (a, b) {
                return a.userGroupName > b.userGroupName;
            });
            var tempGroupName = users[0].userGroupName;
            var tempGroupId = users[0].userGroupId;
            var tempMark = users[0].mark;
            var userArray = [];
            var count = 0;
            users.forEach(function (user) {
                count++;
                if (user.userGroupName !== tempGroupName) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        users: userArray
                    });
                    userArray = [];
                    tempGroupName = user.userGroupName;
                    tempGroupId = user.userGroupId;
                    tempMark = user.mark;
                }
                var bytes = CryptoJS.AES.decrypt(user.password, config.KEY);
                var dePass = bytes.toString(CryptoJS.enc.Utf8);

                userArray.push({
                    id: user._id,
                    userId: user.userId,
                    password: dePass,
                    phone: user.phone
                });
                if (count === users.length) {
                    groupArr.push({
                        groupName: tempGroupName,
                        groupId: tempGroupId,
                        mark: tempMark,
                        users: userArray
                    });
                }
            });
        }

        Spot.find({
            managerId: req.user.managerId
        }, function (err, spots) {
            var spotGroupArr = [];
            if (spots && spots.length > 0) {
                spots.sort(function (a, b) {
                    return a.spotGroupName > b.spotGroupName;
                });
                var tempGroupName = spots[0].spotGroupName;
                var tempGroupId = spots[0].spotGroupId;
                var tempMark = spots[0].mark;
                var spotArray = [];
                var count = 0;
                spots.forEach(function (spot) {
                    count++;
                    if (spot.spotGroupName !== tempGroupName) {
                        spotGroupArr.push({
                            groupName: tempGroupName,
                            groupId: tempGroupId,
                            mark: tempMark,
                            spots: spotArray
                        });
                        spotArray = [];
                        tempGroupName = spot.spotGroupName;
                        tempGroupId = spot.spotGroupId;
                        tempMark = spot.mark;
                    }

                    spotArray.push({
                        id: spot._id,
                        spotName: spot.spotName,
                        lat: spot.lat,
                        lon: spot.lon
                    });
                    if (count === spots.length) {
                        spotGroupArr.push({
                            groupName: tempGroupName,
                            groupId: tempGroupId,
                            mark: tempMark,
                            spots: spotArray
                        });
                    }
                });
            }

            res.render(route_config.user_render_maps, {
                roomName: req.user.managerId,
                userName : req.user.userId,
                groupUsers: groupArr,
                groupSpots: spotGroupArr,
                errors: req.flash(route_config.ERROR),
                success: req.flash(route_config.SUCCESS),
                isAuthenticated: req.isAuthenticated(),
                isManager: req.user.role == 2,
                marks: config.marks,
                managerId: req.user.managerId,
                adminId: req.user.adminId,
                userSession: req.flash('userSession')
            });
        });
        // Spot.findOne({'managerId': req.user.userId}, function(err, spot){
        //     if (err || !spot) {
        //         res.end(JSON.stringify({
        //             success: false,
        //             message: Language.error_get_mark
        //         }));
        //     } else{ 
        //         res.end(JSON.stringify({
        //             success: true,
        //             mark: spot.mark,
        //             message: ''
        //         }));
        //     }
        // });
    })
}