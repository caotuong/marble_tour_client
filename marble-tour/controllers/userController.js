'use strict';

var User = require('../models/user');
var CryptoJS = require("crypto-js");
var config = require('../config');
var Language = require('../config/language');
var ObjectId = require('mongoose').Types.ObjectId;
// Insert user
exports.insert = function(req, res) {
    var credentials = {
        'userId': req.body.userId,
        'password': req.body.password,
        'affiliation': req.body.affiliation,
        'email':req.body.email,
        'phone':req.body.phone,
        'activePriod':req.body.activePriod,
        'loginStatus':req.body.loginStatus,
        'mark': req.body.mark,
        'userGroupId': req.body.userGroupId,
        'userGroupName': req.body.userGroupName,
        'role': req.body.role,
        'adminId': req.body.adminId,
        'managerId': req.body.managerId,
        'status': req.body.status
    };
    
    if(credentials.userId === '' || credentials.password === ''){

        res.end(JSON.stringify({
            message: Language.invalid_username_password
        }));
    } else {
        // Check if the username already exists for non-social account
        User.findOne({'userId': req.body.userId}, function(err, user){
            if(err) {
                res.end(JSON.stringify({
                    message: Language.error_create + err.message
                }));
            }
            if(user){
                res.end(JSON.stringify({
                    message: Language.invalid_username_password
                }));
            }else{
                User.create(credentials, function(err, newUser){
                    if(err) {
                        res.end(JSON.stringify({
                            message: Language.error_create + err.message
                        }));
                    }

                    res.end(JSON.stringify({
                        message: Language.user + Language.has_been_created
                    }));
                });
            }
        });
    }
};
// Get all users
exports.getAllUsers = function(req, res) {
    var credentials = {
        'userId': req.body.userId,
        'password': req.body.password,
        'affiliation': req.body.affiliation,
        'email':req.body.email,
        'phone':req.body.phone,
        'activePriod':req.body.activePriod,
        'loginStatus':req.body.loginStatus,
        'mark': req.body.mark,
        'userGroupId': req.body.userGroupId,
        'userGroupName': req.body.userGroupName,
        'role': req.body.role,
        'adminId': req.body.adminId,
        'managerId': req.body.managerId,
        'status': req.body.status,
        'create':	req.body.create,
        'update':	req.body.update
    };
    
    
    User.find({}, function(err, users) {
        var userMap = {};
    
        users.forEach(function(user) {
          userMap[user._id] = user;
        });
    
        res.send(userMap);  
    });
};

//Manager Group/user
exports.createGroupUser = function(req, res){
    if (req.method == 'GET') {
        renderViewUserGroup(req, res);
    }
    var credentials = {
        'userId': req.body.userId,
        'password': req.body.password,
        'phone':req.body.phone,
        //'mark': req.body.mark,
        //'userGroupName': req.body.userGroupName,
        'role': 1
    };
    
    if(credentials.userId === '' || credentials.password === ''){
        res.end(JSON.stringify({
            message: Language.invalid_username_password
        }));
    }else{
        // Check if the username already exists for non-social account
        User.findOne({'userId': req.body.userId}, function(err, user){
            if(err) {
                res.end(JSON.stringify({
                    message: Language.error_create + Language.group
                }));
            }
            if(user){
                res.end(JSON.stringify({
                    message: Language.invalid_username_password
                }));
            }else{
                User.create(credentials, function(err, newUser){
                    if(err) {
                        res.end(JSON.stringify({
                            message: Language.error_create + Language.group
                        }));
                    }
                });
            }
        });
    }
};

exports.deleteGroup = function(req, res){
    var credentials = {
        'userGroupName': req.body.userGroupName,
    };
    var usersInGroup = {};

    if(credentials.userGroupName === ''){
        res.end(JSON.stringify({
            message: Language.error_groupname_empty
        }));
    }else{
        User.find({'userGroupName': req.body.userGroupName}, function(err, users){
            if(err) {
                res.end(JSON.stringify({
                    message: Language.error_find_groupname
                }));
            }
            if(users){
                res.end(JSON.stringify({
                    message: Language.invalid_groupname
                }));
            }else{
                //remove list users in group
                users.forEach(function(user) {
                    user.remove(function(err) {
                        if(err) { 
                            res.end(JSON.stringify({
                                message: Language.error_find_groupname
                            }));
                        }
                      });
                  });

                  res.end(JSON.stringify({
                    message: Language.group + Language.deleted
                }));
            }
        });
    }
};

 // Manage user
exports.manageUser = function(req, res, next) {
    res.render(manger_user_render, {isAuthenticated: req.isAuthenticated()});
};

exports.getUserByUserId = function(req, res, next) {
    User.findOne({'_id': new ObjectId(req.params.id)}, function(err, user){
        if(err) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_user
            }));
        }
        if(user){
            var bytes  = CryptoJS.AES.decrypt(user.password, config.KEY);
            var dePass = bytes.toString(CryptoJS.enc.Utf8);
            user.password = dePass;
            res.end(JSON.stringify({
                success: true,
                user: user
            }));
        } else {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_user
            }));
        }
        
    });
}

exports.getGroupNameById = function(req, res, next) {
    User.findOne({userGroupId: req.params.groupId}, function(err, user){
        if (err) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_groupname
            }));
        }
        if (user) {
            res.end(JSON.stringify({
                success: true,
                userGroupName: user.userGroupName,
                userGroupId: user.userGroupId,
                mark: user.mark,
                message: ''
            }));
        } else {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_groupname
            }));
        }
    });
};

exports.getMarkerByGroupName = function(req, res, next) {
    User.findOne({userGroupName: req.params.groupName}, function(err, user){
        if (err || !user) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_mark
            }));
        } else{ 
            res.end(JSON.stringify({
                success: true,
                mark: user.mark,
                message: ''
            }));
        }
    });
};
