'use strict';

var User = require('../models/user');
var route_config = require('../routes/config');
var validator = require('../utils/validator');
var CryptoJS = require("crypto-js");
var Language = require('../config/language');
var config = require('../config');
var ObjectId = require('mongoose').Types.ObjectId;
// Show list contractors in table
exports.listContractors = function(req, res, next) {

    var conditions ={
        role: 1,//manager
        adminId: req.user.userId
    };
    
    var listManagers = [];
    var count = 0;
    
    User.find(conditions, function(err, users) {

        users.forEach(function(user) {
            count += 1;
            var bytes  = CryptoJS.AES.decrypt(user.password, config.KEY);
            var dePass = bytes.toString(CryptoJS.enc.Utf8);
            var data = {
                'id': user._id,
                'count': count,
                'userId': user.userId,
                'password': dePass,
                'affiliation': user.affiliation,
                'loginStatus': user.loginStatus,
                'email':user.email,
                'phone':user.phone,
                'activePeriod': user.activePeriod ? formatDate(user.activePeriod) : '',
                'loginStatus':user.loginStatus
            };
            listManagers.push(data);
        });
        
        res.render(route_config.admin_render_manager_user, {
            listManagers: listManagers,
            errors: req.flash(route_config.ERROR),
            success: req.flash(route_config.SUCCESS),
            isAuthenticated: req.isAuthenticated(),
            adminId: req.user.userId,
            managerSession: req.flash('manager')
        });

    });


}

// Create contractor
exports.createContractor = function(req, res, next) {
    var credentials = {
        'userId': req.body.userId,
        'password': req.body.password,
        'email': req.body.email,
        'phone':req.body.phone,
        'affiliation': req.body.affiliation,
        'activePeriod': req.body.activePeriod === '' ? null : new Date(req.body.activePeriod),
        'role': 1,
        'loginStatus': 1,
        'adminId': req.body.adminId,
        'managerId': req.body.userId
    };

    
    if (!validator.validateContractor(credentials, req, res)) {
        req.flash('manager', {
            userId: credentials.userId,
            password: credentials.password,
            email: credentials.email,
            phone: credentials.phone,
            affiliation: credentials.affiliation,
            activePeriod: credentials.activePeriod
        });
        res.redirect(route_config.admin  + route_config.admin_list_constractor);
    } else {
        // Check if the username already exists for non-social account
        User.findOne({'userId': req.body.userId}, function(err, user){
            if(err) {
                console.log(err);
                req.flash(route_config.ERROR, Language.error_create_contractor);
                res.redirect(route_config.admin  + route_config.admin_list_constractor);
            }
            if(user){
                console.log(err);
                req.flash(route_config.ERROR, Language.error_duplicate_userid);
                res.redirect(route_config.admin  + route_config.admin_list_constractor);
            } else {
                User.create(credentials, function(err, newUser){
                    if(err) {
                        console.log(err);
                        
                        req.flash(route_config.ERROR, Language.error_create_contractor);
                        res.redirect(route_config.admin + route_config.admin_list_constractor);
                    } else {
                        req.flash(route_config.SUCCESS, Language.contractor + Language.has_been_created);
                        res.redirect(route_config.admin  + route_config.admin_list_constractor);
                    }
                });
            }
        });

    }
}

// Delete contractor
exports.deleteContractor = function(req, res, next) {
    var credentials = {
        'userId': req.params.userId,   
    };
    
    if(credentials.userId === '') {
        req.flash(route_config.ERROR, Language.error_missing_userid);
        res.redirect(route_config.admin + route_config.admin_list_constractor);
    } else {
        // Check if the username already exists for non-social account
        User.findOne({'userId': req.params.userId}, function(err, user){
            if(err) {
                req.flash(route_config.ERROR, Language.error_delete_contractor);
                res.redirect(route_config.admin + route_config.admin_list_constractor);
            }
            if(user){
               user.remove(function(err) {
                   if(err) { 
                       req.flash(route_config.ERROR, Language.error_delete_contractor);
                       res.redirect(route_config.admin + route_config.admin_list_constractor);
                   } else {
                       req.flash(route_config.SUCCESS, Language.success_delete);
                       res.redirect(route_config.admin + route_config.admin_list_constractor);
                   }
                });
            } else {
                req.flash(route_config.SUCCESS, Language.error_cannotfound_user);
                res.redirect(route_config.admin + route_config.admin_list_constractor);
            }
            
        });
    }
};

// Get contractor by Id
exports.getContractor = function(req, res, next) {
    User.findOne({'_id': new ObjectId(req.params.id)}, function(err, user){
        if(err) {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_contractor
            }));
        }
        if(user){
            var bytes  = CryptoJS.AES.decrypt(user.password, config.KEY);
            var dePass = bytes.toString(CryptoJS.enc.Utf8);
            user.password = dePass;
            var formatActiveP = formatDate(user.activePeriod);
            user.userGroupName = formatActiveP;//muon
            res.end(JSON.stringify({
                success: true,
                user: user
            }));
        } else {
            res.end(JSON.stringify({
                success: false,
                message: Language.error_get_contractor
            }));
        }
        
    });
}

// Update contractor
exports.updateContractor = function(req, res, next) {
    console.log(req.params);
    var credentials = {
        'userId': req.body.userId,
        'password': req.body.password,
        'email': req.body.email,
        'phone':req.body.phone,
        'affiliation': req.body.affiliation,
        'activePeriod': req.body.activePeriod === '' ? null : new Date(req.body.activePeriod),
        'role': 1,
        'loginStatus': 1
    };

    User.findOne({'_id': new ObjectId(req.params.id)}, function(err, user){
        if(err) {
            req.flash(route_config.ERROR, Language.error_update_contractor);
            res.redirect(route_config.admin + route_config.admin_list_constractor);
        }
        if(user){
            if (!validator.validateContractor(credentials, req, res)) {
                res.redirect(route_config.admin + route_config.admin_list_constractor);
            } else {
                User.find({managerId: user.userId}, function(err, musers) {
                    musers.forEach(function(muser) {
                        muser.managerId = credentials.userId;
                        muser.save();
                    });
                });

                user.userId = credentials.userId;
                user.managerId = credentials.userId;
                user.email = credentials.email;
                user.password = credentials.password;
                user.phone = credentials.phone;
                user.activePeriod = credentials.activePeriod;
                var loginStatus = req.body.loginStatus ? 1 : 0;
                user.loginStatus = loginStatus;
                user.affiliation = credentials.affiliation;
                user.save(function(err) {
                    if (err) {
                        console.log(err);
                        req.flash(route_config.ERROR, Language.error_update_contractor);
                        res.redirect(route_config.admin + route_config.admin_list_constractor);
                    } else {
                        req.flash(route_config.SUCCESS, Language.success_update);
                        res.redirect(route_config.admin + route_config.admin_list_constractor);
                    }
                });
            }


        } else {
            req.flash(route_config.ERROR, Language.error_update_contractor);
            res.redirect(route_config.admin + route_config.admin_list_constractor);
        }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}