var eps = 0.000001; // 許容値；

function isAccessBlock(marblePath, prop, start, end) {
  //0:lng 1:lat
  // 内在判定
  var maxLat, maxLng, minLat, minLng;

  if (start.lat > end.lat) {
    maxLat = start.lat;
    minLat = end.lat;
  } else {
    maxLat = end.lat;
    minLat = start.lat;
  }

  if (start.lng > end.lng) {
    maxLng = start.lng;
    minLng = end.lng;
  } else {
    maxLng = end.lng;
    minLng = start.lng;
  }

  if ((minLat < prop.maxLat && prop.minLat < maxLat) &&
    (minLng < prop.maxLng && prop.minLng < maxLng)) {
    if (IsPointOnPolygon(marblePath, start)) {
      return true;
    } else if (IsPointOnPolygon(marblePath, end)) {
      return true;
    }
    return (IsLineClossPolygon(marblePath, start, end));
  }

  return false;

}

function IsPointOnPolygon(marblePath, target) {
  var crossCounter = 0;

  var point0 = {
    "lat": (marblePath[0][1]),
    "lng": (marblePath[0][0])
  };

  var cross0x = (target.lat < point0.lat);
  var cross0y = (target.lng < point0.lng);

  var length = marblePath.length;

  for (var i = 1; i <= length; i++) {
    var point1 = marblePath[i % length];
    var cross1x = (target.lat < point1.lat);
    var cross1y = (target.lng < point1.lng);

    if (cross0y !== cross1y) {
      // 線分は0を横切る可能性あり
      if (cross0x === cross1x) {
        // 線分の2端点は対象点に対して両方右か左にある
        if (cross0x) {
          // 完全に右。⇒線分はレイを横切る
          crossCounter += cross0y ? -1 : 1;
        }
      } else {
        // レイと交差するかどうか、対象点と同じ高さで、対象点の右で交差するか、左で交差するかを求める。
        if (target.lat < (point0.lat + (point1.lat - point0.lat) *
            (target.lng - point0.lng) / (point1.lng - point0.lng))) { // 線分は、対象点と同じ高さで、対象点の右で交差する。⇒線分はレイを横切る
          crossCounter += (cross0y ? -1 : 1); // 上から下にレイを横切るときには、交差回数を１引く、下から上は１足す。
        }
      }
    }
    // 次の判定のために
    point0 = point1;
    cross0x = cross1x;
    cross0y = cross1y;
  }
  // クロスカウントがゼロのとき外、ゼロ以外のとき内。
  return (crossCounter !== 0);
}

function Segment(p1, p2) {
  //if (p1[0] === undefined) {
    this.start = {
      "lat": (p1.lat),
      "lng": (p1.lng)
    };
    this.vector = {
      "lat": (p2.lat - p1.lat),
      "lng": (p2.lng - p1.lng)
    };
  // } else {
  //   this.start = {
  //     "lat": (p1[1]),
  //     "lng": (p1[0])
  //   };
  //   this.vector = {
  //     "lat": (p2[1] - p1[1]),
  //     "lng": (p2[0] - p1[0])
  //   };
  // }
}

//Segment.start = function(){
//    return this.start;
//}

//Segment.vector = function () {
//    return this.vector;
//}


function GetVectorCrossProduct(v0, v1) {
  return v0.lat * v1.lng - v0.lng * v1.lat;
}

function IsLineClossPolygon(marblePath, start, end) {
  var s0 = new Segment(start, end);

  var point0 = {
    "lat": (marblePath[0][1]),
    "lng": (marblePath[0][0])
  };
  var length = marblePath.length;
  for (var i = 1; i <= length; i += 1) {
    var point1 = {
      "lat": (marblePath[i % length][1]),
      "lng": (marblePath[i % length][0])
    };
    var s1 = new Segment(point0, point1);

    // 交差していない、// 次の判定のために設定
    var s0_s1Cross = GetVectorCrossProduct(s0.vector, s1.vector);
    // 平行
    if (s0_s1Cross === 0.0)
      return false;

    var v = {
      "lat": s1.start.lat - s0.start.lat,
      "lng": s1.start.lng - s0.start.lng
    };

    var v_s0Cross = GetVectorCrossProduct(v, s0.vector);
    var v_s1Cross = GetVectorCrossProduct(v, s1.vector);

    var t0 = v_s1Cross / s0_s1Cross;
    var t1 = v_s0Cross / s0_s1Cross;

    if (t0 + eps < 0 || t0 - eps > 1 || t1 + eps < 0 || t1 - eps > 1) {
      // Log.d("tag", t0 + ": " + t1);
      // 交差していない、// 次の判定のために設定
      point0 = point1;
    } else {
      return true;
    }
  }
  return false;
}