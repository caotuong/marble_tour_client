
var marker={};
var marker_id = undefined;

//ロード時の実行イベント
map.on('load', function () {

  //新しいspot追加用
  map.on('click', function (e) {
    console.log("map:" + e);

    if (isMakerClick ||
      $("#spotGroupName").val() == "" ||
      $("#mark").val() == "" ||
      $("#spotName").val() == "") {
      isMakerClick = false;
      return;
    }

    console.log("map click:" + e);

    let marker_id = getMakerID();

    if (marker_id == null) {
      return;
    }

    // create a DOM element for the marker
    let marker = document.createElement('div');
    marker.className = 'marker';
    marker.style.backgroundImage = 'url(' + $("#mark").val() + ')';
    marker.style.width = 32 + "px ";
    marker.style.height = 44 + "px ";
    marker.id = marker_id;

    marker.addEventListener('click', function () {
      window.alert("hogehoge");
    });

    if (marker[marker_id] == undefined) {
      // add marker to map
      marker[marker_id] = new mapboxgl.Marker(marker, {offset: [-32 / 2, -44]})
        .setLngLat(e.lngLat)
        .addTo(map);
    } else {
      marker[marker_id].setLngLat(e.lngLat);
    }

    //テーブルへパラメータの設定
    $("#lat").val(e.lngLat.lat);
    $("#lon").val(e.lngLat.lng);
  });

})
