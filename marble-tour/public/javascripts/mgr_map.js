//変数の定義
mapboxgl.accessToken = 'pk.eyJ1IjoibWFzYWZ1bWktdGFrZSIsImEiOiJjaXpyNjYxaGwwMDQ0MnFsOWJ1eW5zemhzIn0.0nqIWvbkv3moGJVaNKODVA';
//接続ブロック用のGeoJsonデータ
var accessBlocks;
//マウスのドラッグ状態フラグ
var isDragging = false;
//マウスカーソルオーバーフラグ
var isCursorOverPoint = false;
//緯度経度情報の表示パネル
var coordinates = document.getElementById('coordinates');
//スタート位置
var startUserPoint = {
  lng: 135.768738,
  lat: 35.010348
};

//移動中のマーカー
var currentMarker;

//マーカーのリスト
var markers = {};
var spotMarker = {};

//MapbleGL地図
var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/masafumi-take/cj3wpv7wi0o6c2rmz8pgiux3w',
  center: startUserPoint,
  zoom: 13
});
//キャンパス
var canvas = map.getCanvasContainer();

//目的地
var Points = {
  target: {
    lng: 135.780917,
    lat: 35.00388
  },
  my: startUserPoint
};

//アイコン接続先のドメイン
var iconHostURL = "/images/"

// マーカークリックフラグ
var isMakerClick = false;
// 選択マーカーのMarkerID
var selectedMarker_id = undefined;

//ロード時の実行イベント
map.on('load', function () {
  //change label language
  map.getStyle().layers.forEach(layer => {
    if (layer.type === 'symbol' && layer.layout && layer.layout['text-field'] !== undefined) {
      map.setLayoutProperty(layer.id, 'text-field', '{name}');
    }
  });
});

//gen uuid
function generateUuid() {
  // https://github.com/GoogleChrome/chrome-platform-analytics/blob/master/src/internal/identifier.js
  // const FORMAT: string = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
  let chars = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".split("");
  for (let i = 0, len = chars.length; i < len; i++) {
    switch (chars[i]) {
      case "x":
        chars[i] = Math.floor(Math.random() * 16).toString(16);
        break;
      case "y":
        chars[i] = (Math.floor(Math.random() * 4) + 8).toString(16);
        break;
    }
  }
  return chars.join("");
}

function getMakerID() {
  var action = $("#formSpotGroup").attr("action").split('/');
  if (action === undefined) {
    //アクションが不定の場合
    return generateUuid();
  }

  if (action[2] === "spot") {
    if (action[4] === undefined) {
      //新規作成なのでUUIDを発行する。
      return generateUuid();
    } else {
      //更新なのでspotIDを返す。
      return action[4];
    }
  }

  //Setup Spot group 
  return null;
}

var marker_id = undefined;

//ロード時の実行イベント
map.on('load', function () {
  //change label language
  map.getStyle().layers.forEach(layer => {
    if (layer.type === 'symbol' && layer.layout && layer.layout['text-field'] !== undefined) {
      map.setLayoutProperty(layer.id, 'text-field', '{name}');
    }
  });

  for (let i = 0; i < groupSpots.length; i = (i + 1) | 0) {
    let markerImage = groupSpots[i].mark;

    for (let j = 0; j < groupSpots[i].spots.length; j = (j + 1) | 0) {
      let spot = groupSpots[i].spots[j];
      let marker = document.createElement('div');
      marker.className = 'marker';
      marker.style.backgroundImage = 'url(' + markerImage + ')';
      marker.style.width = 32 + "px ";
      marker.style.height = 44 + "px ";
      marker.id = spot.id;

      let latlng = {
        lat: spot.lat,
        lng: spot.lon
      };

      let pop = new mapboxgl.Popup({offset:[0,-22]})
        .setLngLat(latlng)
        .setText(spot.spotName);

      marker.addEventListener('click', function (e) {
        isMakerClick = true;
        if(e.srcElement.id !== name)
        {
          targetId = e.srcElement.id;
          var marker = markers[e.srcElement.id];
          Points.target = marker.getLngLat();
          updateAccessBlocks();
        }
      });

      markers[spot.id] = new mapboxgl.Marker(marker, [-32 / 2, -44])
        .setLngLat(latlng)
        .setPopup(pop)
        .addTo(map);
    }
  }

})
