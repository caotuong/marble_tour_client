// 1.イベントとコールバックの定義
var chat = io("http://localhost:3001/chat");
var loc = io("http://localhost:3001/location");
var isEnter = false;
var name = "";
var room = "";
var group = "";
var userLocations = "";
var myLocation = {
  lng: 135.76342743636894,
  lat: 35.02295265626702
};

var targetId = "6";

var iconHostURL = "/images/"
var isDragging = false;

//目的地
var Points = {
  target: myLocation,
  my: startUserPoint
};

//カスタムマーカー
var userPoint;

function createUserPinMarker(user, group) {
  // create a DOM element for the marker
  var marker = document.createElement('div');
  marker.className = 'marker';
  if (user.userId !== name) {
    marker.style.backgroundImage = 'url(' + group.mark + ')';
  } else {
    marker.style.backgroundImage = 'url(' + iconHostURL + "my.png" + ')';
  }
  marker.style.width = "32px";
  marker.style.height = "44px";
  marker.id = user.userId;

  marker.addEventListener('click', function (e) {
    //      map.on('click', function (e) {
    console.log(e);
    if (e.srcElement.id !== name) {
      targetId = e.srcElement.id;
      var marker = markers[e.srcElement.id];
      Points.target = marker.getLngLat();

  //    loc.emit("client_to_server_location", {
  //      user: name,
  //      group: group,
  //      room: selectRoom,
  //      latlng: Points.my,
  //      targetId: targetId,
  //      targetLatLng: Points.target
  //    });
      
      updateAccessBlocks();
    }
    //      });
  });

  let pop = new mapboxgl.Popup({
      offset: [0, -22]
    })
    .setLngLat(myLocation)
    .setText(user.userId);

  // add marker to map
  markers[marker.id] =
    new mapboxgl.Marker(marker, {
      offset: [0, 0]
    })
    .setLngLat(myLocation)
    .setPopup(pop)
    .addTo(map);

  return markers[marker.id];
}

function onMove(e) {
  if (!isDragging) return;
  var coords = e.lngLat;

  // Set a UI indicator for dragging.
  canvas.style.cursor = 'grabbing';

  currentMarker.setLngLat(coords);
}

function init() {
  for (index in groupUsers) {
    for (userIndex in groupUsers[index].users) {
      createUserPinMarker(groupUsers[index].users[userIndex], groupUsers[index]);
    }
  }
}


function updateAccessBlocks() {
  //管理者の場合はBlockを表示しない。
  if (isRoomMaster()) {
    return;
  }

  let start = Points["my"];
  let end = Points["target"];

    $.post("http://54.65.163.14:3002/api/get_marbles", {
    "lat1": start.lat,
    "lng1": start.lng,
    "lat2": end.lat,
    "lng2": end.lng
  }, function (data) {

    accessBlocks = data;
    var copyObj = JSON.parse(JSON.stringify(accessBlocks));

    //データを書き換えて再度セットする
    for (i = accessBlocks.features.length - 1; i >= 0; i--) {
      var prop = accessBlocks.features[i].properties;
      var marblePath = accessBlocks.features[i].geometry.coordinates[0];
      if (!isAccessBlock(marblePath, prop, start, end)) {
        copyObj.features.splice(i, 1);
      }
    }

    if (map.getSource('sample') === undefined) {
      //区画データのロード
      map.addSource('sample', {
        "type": "geojson",
        "data": copyObj
      });

      //区画データの表示
      map.addLayer({
        "id": "sample-fill",
        "type": "fill",
        "source": "sample",
        "paint": {
          "fill-color": "#f00",
          "fill-opacity": 0.3
        },
      }, 'building');

      map.addLayer({
        "id": "sample-line",
        "type": "line",
        "source": "sample",
        "paint": {
          "line-color": "#840",
          "line-width": 1
        },
      });

    } else {
      map.getSource('sample').setData(copyObj);
    }
  });
}

function isRoomMaster() {
  return name === selectRoom;
}

//初期化
$(function () {
  //自分の表示名
  name = userName;
  //room名
  room = roomName;
  selectRoom = room;
  group = "work";

  if (isRoomMaster()) {
    Points.my == undefined;
  }

  init();
  //name = message;
  var entryMessage = "[" + name + "]" + "さんが入室しました。";
  //                chat.emit("client_to_server_join", {value : selectRoom});
  chat.emit("client_to_server_join", {
    room: selectRoom,
    user: name,
  });

  loc.emit("client_to_server_join", {
    room: selectRoom,
    user: name,
    group: group,
    targetId: targetId,
    latlng: Points.my,
    targetLatLng: Points.target
  });

  // C05. client_to_server_broadcastイベント・データを送信する
  chat.emit("client_to_server_broadcast", {
    value: entryMessage,
    room: selectRoom
  });
  // C06. client_to_server_personalイベント・データを送信する
  chat.emit("client_to_server_personal", {
    user: name,
    room: selectRoom
  });

  ///////////////////////イベント
  //Messageの送信ボタンの押下
  $("#sendMessageBtn").click(function (e) {
    say(e);
  });

  //Key制御
  $("#messageForm").keydown(function (e) {
    if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
      say(e);
      return false;
    } else {
      return true;
    }
  });

  if (!isRoomMaster()) {
    document.body.addEventListener('keydown', function (e) {
      let coords = Points.my;
      if (selectRoom !== name) { //管理者の場合は位置を表示。変更しない
        const addLat = 0.001;
        const addLng = 0.001;
        if (e.which === 37) { // left
          coords.lng = coords.lng - addLng;
          Points.my = coords;
          e.preventDefault();
        } else if (e.which === 39) { // right
          coords.lng = coords.lng + addLng;
          Points.my = coords;
          e.preventDefault();
        } else if (e.which === 38) { // up
          coords.lat = coords.lat + addLng;
          Points.my = coords;
          e.preventDefault();
        } else if (e.which === 40) { // down
          coords.lat = coords.lat - addLng;
          Points.my = coords;
          e.preventDefault();
        }
        markers[name].setLngLat(Points.my);
      }

      loc.emit("client_to_server_location", {
        user: name,
        group: group,
        room: selectRoom,
        latlng: coords,
        targetId: targetId,
        targetLatLng: Points.target
      });
    }, true);
  }

});

//
// map.on('click', function (e) {
//   console.log("map:" + e.lngLat);

//   loc.emit("client_to_server_location", {
//     user: name,
//     room: selectRoom,
//     latlng: e.lngLat,
//   });
// });

loc.on("server_to_client", function (data) {
  console.log(data);
  userLocations = data.value;
  applyMap(data.value);
});




//Messageの送信
function say(e) {
  var message = $("#messageForm").val();
  // if (msg == "") return;

  // $("#messageForm").val("");

  // var data = {
  //   cmd: "msg",
  //   user: user,
  //   value: msg
  // };

  // socket.emit("publish", data);
  // console.log(data);

  var selectRoom = room;
  $("#messageForm").val("");
  //if (isEnter) {
  message = "[" + name + "]: " + message;
  // C03. client_to_serverイベント・データを送信する
  chat.emit("client_to_server", {
    value: message,
    room: selectRoom
  });
  //}

  e.preventDefault();
}
// function ChangeLocation(LatLng) {
//   var selectRoom = $("#rooms").val();

//   if (name !== undefined) {
//     //位置の送信
//     loc.emit("client_to_server_location", {
//       room: selectRoom,
//       user: name,
//       group: "work",
//       latlng: LatLng
//     });
//   }
// }

chat.on("server_to_client", function (data) {
  addMessage(data)
});



// $("form").submit(function (e) {
//   var message = $("#msgForm").val();
//   var selectRoom = room;
//   $("#msgForm").val("");
//   if (isEnter) {
//     message = "[" + name + "]: " + message;
//     // C03. client_to_serverイベント・データを送信する
//     chat.emit("client_to_server", {
//       value: message,
//       room: selectRoom
//     });
//   }
//   e.preventDefault();
// });

//Messageの追加
function addMessage(data) {
  //Titmestamp
  const date = new Date();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();
  let msg = "";
  if (data.value === undefined) {
    msg = "<div><b>" + ('00' + hour).slice(-2) + ":" + ('00' + minute).slice(-2) + "</b> " + data + "</div>";
  } else {
    msg = "<div><b>" + ('00' + hour).slice(-2) + ":" + ('00' + minute).slice(-2) + "</b> " + data.value + "</div>";
  }
  $("#messageView").append(msg);
}