function applyMap(value) {
  if (value == "connected location socket!") {
    console.log(value);
    return;
  }

  let targetId ="";
  for (let group in value) {
    for (let user in value[group]) {
      let prop = value[group][user];
      let marker = markers[user];

      if(user === room)continue;

      marker.setLngLat(prop.latlng);
      if (user === name) {
        targetId = prop.target;
      }
    }
  }

  let target = markers[targetId];
  if (target !== undefined) {
    Points.target = target.getLngLat();
  }
  
  updateAccessBlocks();
  console.log("user:" + value);
}