var isUpdatingGroup = false;
var isInsertingUser = true;

var isUpdatingSpotGroup = false;
var isInsertingSpot = true;

var opts = {
    lines: 13 // The number of lines to draw
  , length: 28 // The length of each line
  , width: 14 // The line thickness
  , radius: 42 // The radius of the inner circle
  , scale: 1 // Scales overall size of the spinner
  , corners: 1 // Corner roundness (0..1)
  , color: '#000' // #rgb or #rrggbb or array of colors
  , opacity: 0.25 // Opacity of the lines
  , rotate: 0 // The rotation offset
  , direction: 1 // 1: clockwise, -1: counterclockwise
  , speed: 1 // Rounds per second
  , trail: 60 // Afterglow percentage
  , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
  , zIndex: 2e9 // The z-index (defaults to 2000000000)
  , className: 'spinner' // The CSS class to assign to the spinner
  , top: '50%' // Top position relative to parent
  , left: '50%' // Left position relative to parent
  , shadow: false // Whether to render a shadow
  , hwaccel: false // Whether to use hardware acceleration
  , position: 'absolute' // Element positioning
}

$(function () {
    $('#datetimepickerDuration').datetimepicker();
});

$(function() {
    $('.dropdown-menu a').click(function() {
        $(this).closest('.dropdown').find('input.editableselect').val($(this).attr('data-value'));
        if($(this).closest('.dropdown').find('input.editableselect').attr('id') == 'userGroupName') {
            onChangeGroup($(this).attr('data-value'));
        } else {
            onChangeSpotGroup($(this).attr('data-value'));
        }
    });
});



/* Prettyify */
$( document ).ready(function() {
    prettyPrint();
});


/* Scrollspy */
var navHeight = $('.navbar').outerHeight(true) + 10

$('body').scrollspy({
    target: '.bs-sidebar',
    offset: navHeight
})


/* Prevent disabled links from causing a page reload */
$("li.disabled a").click(function() {
    event.preventDefault();
});


/* Adjust the scroll height of anchors to compensate for the fixed navbar */
window.disableShift = false;
var shiftWindow = function() {
    if (window.disableShift) {
        window.disableShift = false;
    } else {
        /* If we're at the bottom of the page, don't erronously scroll up */
        var scrolledToBottomOfPage = (
            (window.innerHeight + window.scrollY) >= document.body.offsetHeight
        );
        if (!scrolledToBottomOfPage) {
            scrollBy(0, -60);
        };
    };
};
if (location.hash) {shiftWindow();}
window.addEventListener("hashchange", shiftWindow);


/* Deal with clicks on nav links that do not change the current anchor link. */
$("ul.nav a" ).click(function() {
    var href = this.href;
    var suffix = location.hash;
    var matchesCurrentHash = (href.indexOf(suffix, href.length - suffix.length) !== -1);
    if (location.hash && matchesCurrentHash) {
        /* Force a single 'hashchange' event to occur after the click event */
        window.disableShift = true;
        location.hash='';
    };
});

// Admin screen
function changeContractor(id) {
    $.getJSON("/admin/contractor/" + id, function( data ) {
        if (data.success) {
            setData(true, data.user);
        }
    });
}
// User user group screen
function changeUserInfo(id) {
    $.getJSON("/users/user/" + id, function( data ) {
        if (data.success) {
            setDataUserGroup(true, data.user);
        }
    });
}

// User spot group screen
function changeSpotInfo(id) {
    $.getJSON("/manager/spot/" + id, function( data ) {
        if (data.success) {
            setDataSpotGroup(true, data.spot);
        }
    });
}
//change user group
function changeGroup(groupId) {
    $.getJSON('/users/group/' + groupId, function( data ) {
        if (data.success) {
            setUpdateGroup(true, data);
        }
    });
}
//change spot group
function changeSpotGroup(groupId) {
    $.getJSON('/manager/spot_group/' + groupId, function( data ) {
        if (data.success) {
            setUpdateSpotGroup(true, data);
        }
    });
}

function setUpdateGroup(isUpdate, groupData) {
    if (isUpdate) {
        isUpdatingGroup = true;
        $('#userGroupName').val(groupData.userGroupName);
        $('#mark').val(groupData.mark);
        $('#cancelUpdateUserGroup').show();
        $('#updateUserGroup').show();
        $('#userId').val('');
        $('#password').val('');
        $('#phone').val('');
        $('#userId').prop('disabled', true);
        $('#password').prop('disabled', true);
        $('#phone').prop('disabled', true);
        $('#formUserGroup').prop('action', '/manager/group/update/' + groupData.userGroupId);
    } else {
        $('#cancelUpdateUserGroup').hide();
        $('#userGroupName').val('');
        $('#updateUserGroup').hide();
        $('#cancelUpdateUserGroup').hide();
        $('#userId').removeAttr('disabled');
        $('#password').removeAttr('disabled');
        $('#phone').removeAttr('disabled');
        $('#formUserGroup').prop('action', '/manager/user/create/');
    }
    $('#mark').removeAttr('disabled');   
    $('#submitUserGroup').prop('disabled', isUpdate);
    $('#updateUserGroup').prop('disabled', !isUpdate);
}

function setUpdateSpotGroup(isUpdate, groupData) {
    if (isUpdate) {
        isUpdatingSpotGroup = true;
        $('#spotGroupName').val(groupData.spotGroupName);
        $('#mark').val(groupData.mark);
        $('#cancelUpdateSpotGroup').show();
        $('#updateSpotGroup').show();
        $('#spotName').val('');
        $('#lat').val('');
        $('#lon').val('');
        $('#lat').prop('disabled', true);
        $('#lon').prop('disabled', true);
        $('#formSpotGroup').prop('action', '/manager/spotgroup/update/' + groupData.spotGroupId);
    } else {
        $('#cancelUpdateSpotGroup').hide();
        $('#spotGroupName').val('');
        $('#updateSpotGroup').hide();
        $('#cancelUpdateSpotGroup').hide();
        $('#lat').removeAttr('disabled');
        $('#lon').removeAttr('disabled');
        $('#formSpotGroup').prop('action', '/manager/spot/create/');
    }
    $('#mark').removeAttr('disabled');   
    $('#submitSpotGroup').prop('disabled', isUpdate);
    $('#updateSpotGroup').prop('disabled', !isUpdate);
}
function setData(isUpdate, user) {
    if (isUpdate) {
        $('#userId').val(user.userId);
        //$('#userId').prop('disabled', true);
        
        $('#email').val(user.email);
        $('#phone').val(user.phone);
        $('#affiliation').val(user.affiliation);
        $('#password').val(user.password);
        $('#activePeriod').val(user.userGroupName);//temp var
        $('#contractorForm').prop('action', '/admin/contractor/update/' + user._id);
        $('#loginStatus').show();
        $('#cancelUpdate').show();
        $('#updateContractor').show();
        
        if (user.loginStatus == 1) {
            $('#loginStatus').prop('checked', true);
        } else {
            $('#loginStatus').prop('checked', false);
        }
    } else {
        $('#userId').val('');
        //$('#userId').removeAttr('disabled');
        $('#password').val('');
        $('#phone').val('');
        $('#email').val('');
        $('#activePeriod').val('');
        $('#affiliation').val('');
        $('#loginStatus').hide();
        $('#cancelUpdate').hide();
        $('#updateContractor').hide();
        $('#contractorForm').prop('action', '/admin/contractor/create/');
    }
    $('#addContractor').prop('disabled', isUpdate);
    $('#updateContractor').prop('disabled', !isUpdate);
}

function setDataUserGroup(isUpdate, user) {
    if (isUpdate) {
        $('#userGroupName').val(user.userGroupName);
        $('#userId').val(user.userId);
        $('#mark').prop('disabled', true);
        $('#phone').removeAttr('disabled');
        $('#password').removeAttr('disabled');
        $('#phone').val(user.phone);
        $('#password').val(user.password);
        $('#mark').val(user.mark);
        $('#cancelUpdateUserGroup').show();
        $('#updateUserGroup').show();
        $('#formUserGroup').prop('action', '/manager/user/update/' + user._id);

    } else {
        $('#cancelUpdateUserGroup').hide();
        $('#userGroupName').val('');
        $('#userId').val('');
        $('#password').val('');
        $('#phone').val('');
        $('#mark').val('');
        $('#updateUserGroup').hide();
        $('#cancelUpdateUserGroup').hide();
        $('#formUserGroup').prop('action', '/manager/user/create/');
    }
    $('#submitUserGroup').prop('disabled', isUpdate);
    $('#updateUserGroup').prop('disabled', !isUpdate);
}

function setDataSpotGroup(isUpdate, spot) {
    if (isUpdate) {
        $('#spotGroupName').val(spot.spotGroupName);
        $('#spotName').val(spot.spotName);
        $('#mark').prop('disabled', true);
        $('#lat').val(spot.lat);
        $('#lon').val(spot.lon);
        $('#mark').val(spot.mark);
        $('#cancelUpdateSpotGroup').show();
        $('#updateSpotGroup').show();
        $('#formSpotGroup').prop('action', '/manager/spot/update/' + spot._id);

    } else {
        $('#cancelUpdateSpotGroup').hide();
        $('#spotGroupName').val('');
        $('#spotName').val('');
        $('#lat').val('');
        $('#lon').val('');
        $('#mark').val('');
        $('#updateSpotGroup').hide();
        $('#cancelUpdateSpotGroup').hide();
        $('#formSpotGroup').prop('action', '/manager/spot/create/');
    }
    $('#submitSpotGroup').prop('disabled', isUpdate);
    $('#updateSpotGroup').prop('disabled', !isUpdate);
}

$('#cancelUpdate').click(function() {
    event.preventDefault();
    setData(false, null);
});

$('#cancelUpdateUserGroup').click(function() {
    event.preventDefault();
    setDataUserGroup(false, null);
    setUpdateGroup(false, null);
});

$('#cancelUpdateSpotGroup').click(function() {
    event.preventDefault();
    setDataSpotGroup(false, null);
    setUpdateSpotGroup(false, null);
});

$('#groupEditableSelect').editableSelect();


$('#formUserGroup').submit(function() {
    $('#mark').removeAttr('disabled');
});

$('#formSpotGroup').submit(function() {
    $('#mark').removeAttr('disabled');
});
//Change user group name
function onChangeGroup(groupName) {
    if (isUpdatingGroup || isInsertingUser) {
        $.getJSON('/users/groupname/' + groupName, function(data){
            if (data.success) {
                $('#mark').val(data.mark);
                $('#mark').prop('disabled', true);
            } else {
                $('#mark').removeAttr('disabled');
            }
        });
    }
}

$('#userGroupName').change(function() {
    onChangeGroup($(this).val());
});
$('#spotGroupName').change(function() {
    onChangeSpotGroup($(this).val());
});

//Change spot group name
function onChangeSpotGroup(spotGroupName) {
    if (isUpdatingSpotGroup || isInsertingSpot) {
        $('#loader').show();
        $.getJSON('/manager/spotgroupname/' + spotGroupName, function(data){
            if (data.success) {
                $('#mark').val(data.mark);
                $('#mark').prop('disabled', true);
            } else {
                $('#mark').removeAttr('disabled');
            }
        });
        $('#loader').hide();
    }
}
$('#groupEditableSelect').on('select.editable-select', function (e) {
    //onChangeGroup();
    onChangeSpotGroup();   
});

$('#groupEditableSelect').change(function(e) {
    //onChangeGroup();
    onChangeSpotGroup();
});

function changeCollapseIcon(obj) {
    if ($(obj).hasClass('fa-caret-right')) {
        $(obj).removeClass('fa fa-caret-right');
        $(obj).addClass('fa fa-caret-down');
    } else {
        $(obj).removeClass('fa fa-caret-down');
        $(obj).addClass('fa fa-caret-right');
    }
}

