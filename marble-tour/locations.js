var _roomParams = {};

function Locations() {}
//ルームの追加、ユーザー
Locations.prototype.Registration = function (roomName, userName, groupName, latLng, targetId, targetLatLng, socketId) {
  require('date-utils');

  if (_roomParams[roomName] === undefined) {
    _roomParams[roomName] = {};
  }
  if (_roomParams[roomName][groupName] === undefined) {
    _roomParams[roomName][groupName] = {};
  }
  if (_roomParams[roomName][groupName][userName] === undefined) {
    _roomParams[roomName][groupName][userName] = {};
  }

  _roomParams[roomName][groupName][userName] = {
    "latlng": latLng,
    "socketId": socketId,
    "online": true,
    "target": targetId,
    "targetLatLng" : targetLatLng,
    "timestamp": new Date().toFormat("YYYY/MM/DD/-HH24:MI:SS"),
  };

  return _roomParams[roomName] /*[groupName]*/ ;
}

Locations.prototype.ToOffline = function (socketId) {
  require('date-utils');

  for (var room in _roomParams)
    for (var group in _roomParams[room])
      for (var user in _roomParams[room][group]) {

        if (_roomParams[room][group][user].socketId === socketId) {
          _roomParams[room][group][user].online = false;
          _roomParams[room][group][user].timestamp = new Date().toFormat("YYYY/MM/DD/-HH24:MI:SS");
        }
      }
}

Locations.prototype.GetAdminSocketId = function (room, group) {
  if (_roomParams[room])
    if (_roomParams[room][group])
      if (_roomParams[room][group][room]) {
        return _roomParams[room][group][room].socketId;
      }
}


//ルーム内のユーザー確認
Locations.prototype.Refer = function (roomName) {
  return JSON.stringify(_roomParams[roomName]);
}

//ルームリストの取得
Locations.prototype.GetRoomList = function () {
  let list = [];

  for (var room in _roomParams) {
    list.push[room];
  }

  return JSON.stringify(list);
}

//SocketIDからUserを取得
Locations.prototype.GetUserProperties = function (socketId) {

  for (var room in _roomParams)
    for (var group in _roomParams[room])
      for (var user in _roomParams[room][group]) {

        if (_roomParams[room][group][user].socketId === socketId) {
          var ret = {};
          ret[user] = _roomParams[room][group][user];
          return JSON.stringify(ret);
        }
      }

  return undefined;
}

//グループ一覧を取得
Locations.prototype.GetGroups = function (roomName) {
  let list = [];

  for (var group in _roomParams[room]) {
    list.push[group];
  }

  return JSON.stringify(list);
}

//自分が所属のグループを取得
Locations.prototype.GetGroups = function (roomeName, userName) {

  for (var group in _roomParams[roomeName]) {
    for (var user in _roomParams[roomeName][group]) {

      if (user === userName)
        return JSON.stringify(gruop);
    }
  }

  // グル-プがない場合-管理者？
  return JSON.stringify([]);
}

//グループ内のユーザーリストを取得
Locations.prototype.GetGroupContainsUser = function (roomName, groupName) {
  return JSON.stringify(_roomParams[roomName][groupName]);
}

//ルーム内のメンバー全員のデータを取得
Locations.prototype.GetGroups = function (roomName) {
  return JSON.stringify(_roomParams[roomName]);
}

module.exports = Locations;