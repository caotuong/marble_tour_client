//ソケットのルーター

function SOCKET_ROUTER(io, loc) {
  var self = this;
  self.handleRouter(io, loc);
}

SOCKET_ROUTER.prototype.handleRouter = function (io, loc) {
  require("./rest_routers/chat")(io, loc);
  //require("./rest_routers/fortune")(io, loc);
  require("./rest_routers/location")(io, loc);
}

module.exports = SOCKET_ROUTER;