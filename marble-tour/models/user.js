'use strict';

var Mongoose 	= require('mongoose');
var uuid = require('uuid/v1');
var CryptoJS = require("crypto-js");
const KEY = 'marble-tour';

var UserSchema = new Mongoose.Schema({
    userId:	{ type: String, required: true, unique: true},
	password:	{ type: String, required: true},
	affiliation:	String,
	email:	String,
	phone:	String,
	activePeriod:	Date,
	loginStatus:	Number,
	mark: String,
	userGroupId: String,
	userGroupName: String,
	role: Number,
	adminId: String,
	managerId: String,
	status: Number,
	create:	{ type: Date, default: Date.now },
	update:	{ type: Date, default: Date.now }
});


UserSchema.pre('save', function(next) {
    var user = this;

    if (!user.isModified('password')) 
        return next();
    
    user.password = CryptoJS.AES.encrypt(user.password, KEY);
    next();
});

UserSchema.methods.validatePassword = function(password, callback) {
   
    // Decrypt 
    var bytes  = CryptoJS.AES.decrypt(this.password, KEY);
    var dePass = bytes.toString(CryptoJS.enc.Utf8);
    callback(null,password == dePass ? true : false);
};


// Create a user model
var userModel = Mongoose.model('users', UserSchema);

module.exports = userModel;