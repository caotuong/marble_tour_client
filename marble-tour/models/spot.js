'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uuid = require('uuid/v1');
var Spotschema = new Schema({
	spotName:	String,
    lat:	Number,
    lon:	Number,
    mark:	String,
    spotGroupId: String,
    spotGroupName:	String,
    adminId:	String,
    managerId:	String,
	create:	{ type: Date, default: Date.now },
	update:	{ type: Date, default: Date.now }
});
 
var Spot = mongoose.model('spots', Spotschema);
module.exports = Spot;